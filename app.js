const express = require('express');
const chalk = require('chalk');
const debug = require('debug')('app');
const morgan = require('morgan');
const path = require('path');

const port = process.env.PORT || 3000;
const app = express();
const sessionsRouter = require('./src/routers/sessionsRouter');
const adminRouter = require('./src/routers/adminRouter');

// app.use(morgan('combined'));
app.use(morgan('tiny'));
app.use(express.static(path.join(__dirname, '/public/')));

app.set('views', './src/views');
app.set('view engine', 'ejs');

app.use('/sessions', sessionsRouter);
app.use('/admin', adminRouter);

app.get('/', (req, res) => {
  // res.send('Hello from my app');
  res.render('index', { title: 'GlobalLogic', data: ['a', 'b', 'c', 'd'] });
});

app.listen(port, () => {
  debug(`Listening on port ${chalk.green(port)}`);
});
