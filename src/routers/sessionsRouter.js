const express = require('express');
const sessionsRouter = express.Router();
// const sessions = require('../data/sessions.json');
const debug = require('debug')('app:sessionRouter');
const { MongoClient, ObjectId } = require('mongodb');

sessionsRouter.route('/').get((req, res) => {
  // res.send('hello sessions');

  // res.render('sessions', {
  //   sessions,
  // });

  const url =
    'mongodb+srv://nodejs:hwPvmdKYwK3Ef3b@clusternodejs.3e16d1i.mongodb.net/?retryWrites=true&w=majority';
  const dbName = 'globallogic';

  (async function mongo() {
    let client;
    try {
      client = await MongoClient.connect(url);
      debug('Connected to the mongo DB');

      const db = client.db(dbName);

      const sessions = await db.collection('sessions').find().toArray();
      res.render('sessions', { sessions });
    } catch (error) {
      debug(error.stack);
    }
    client.close();
    debug('Connection to the mongo DB closed');
  })();
});

sessionsRouter.route('/:id').get((req, res) => {
  const id = req.params.id;

  const url =
    'mongodb+srv://nodejs:hwPvmdKYwK3Ef3b@clusternodejs.3e16d1i.mongodb.net/?retryWrites=true&w=majority';
  const dbName = 'globallogic';

  (async function mongo() {
    let client;
    try {
      client = await MongoClient.connect(url);
      debug('Connected to the mongo DB');

      const db = client.db(dbName);

      const session = await db
        .collection('sessions')
        .findOne({ _id: new ObjectId(id) });

      res.render('session', {
        session,
      });
    } catch (error) {
      debug(error.stack);
    }
    client.close();
    debug('Connection to the mongo DB closed');
  })();
});

module.exports = sessionsRouter;
